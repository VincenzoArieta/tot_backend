'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const { Client } = require('pg');

const ALL_TABLES = 5;
const DEFAULT_ITEMS_PER_PAGE = 10;
const TABLE_SEATS = 4;
const ALL_SEATS = TABLE_SEATS * ALL_TABLES;
const OPENING_TIME_HOUR = 19;
const LAST_AVAILABLE_TIME_HOUR = 23;
const TOT_RESERVATION = "tot_reservation";
const TOT_USER = "tot_user";

const PORT = process.env.PORT || 8080;

const client = new Client({
  user: process.env.PGUSER,
  password: process.env.PGPASSWORD,
  database: process.env.PGDATABASE
});

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(bodyParser.raw({
  limit: '200mb',
  verify: function(req, res, buf) {
    req.rawBody = buf;
  }
}));

app.get("/getReservations", async (req, res) => {
  let fromTimestamp = req.query.from; // date in milliseconds
  let toTimestamp = req.query.to; // date in milliseconds
  let page = req.query.page || 1; // Optional param
  let limit = req.query.limit || DEFAULT_ITEMS_PER_PAGE; // Optional param

  // I am assuming that the date range, whenever supplied, is valid
  if (!fromTimestamp || !toTimestamp) {
    sendErrorResponse(res, "Request failed. Missing information");
  } else {
    // Parse timestamps
    let fromDateString = formatTimestamp(Number(fromTimestamp));
    let toDateString = formatTimestamp(Number(toTimestamp));
    let query = `SELECT *
    FROM ${TOT_RESERVATION}
    WHERE date_start BETWEEN '${fromDateString}' AND '${toDateString}'
    LIMIT ${limit}
    OFFSET ${(page - 1) * limit}`;

    client.query(query)
    .then((results) => {
      res.send(results.rows);
    })
    .catch((error) => {
      let errorMessage = `getReservations failed. ${error.message || 'Unknown reason'}`;
      sendErrorResponse(res, errorMessage)
    });
  }

});

app.post("/createUser", async (req, res) => {
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  let email = req.body.email;

  if (!firstName || !lastName || !email) {
    sendErrorResponse(res, "Request failed. Missing information");
  } else {
    let queryString = `INSERT INTO ${TOT_USER} (email, first_name, last_name) VALUES ('${[email, firstName, lastName].join("','")}') RETURNING *`;

    client.query(queryString)
    .then((results) => {
      res.send(results.rows[0]);
    })
    .catch((error) => {
      let errorMessage = `createUser failed. ${error.message || 'Unknown reason'}`;
      sendErrorResponse(res, errorMessage)
    });
  }

});

app.post("/createReservation", async (req, res) => {
  let userEmail = req.body.userEmail; // String
  let guests = req.body.guests; // Number
  let fromTimestamp = req.body.fromDate; // Milliseconds

  if (!userEmail || !isGuestsNumberValid(guests) || !isReservationDateValid(fromTimestamp)) {
    let errorMessage = "Reservation date is not valid";

    if (!userEmail) {
      errorMessage = "No user supplied"
    } else if (!isGuestsNumberValid(guests)) {
      errorMessage = (guests > ALL_SEATS) ? `The restaurant does not have so many seats (Total seats are ${ALL_SEATS})` : "No guests specified";
    }

    sendErrorResponse(res, `Request failed. ${errorMessage}`);
  } else {
    let requiredTables = (guests <= TABLE_SEATS) ? 1 : Math.ceil(guests / TABLE_SEATS);
    if (requiredTables > ALL_TABLES) throw new Error(`You'd need more tables than the restaurant has. The restaurant has ${ALL_TABLES} tables only.`);

    let fromDateString = formatTimestamp(Number(fromTimestamp));
    let toDate = new Date(Number(fromTimestamp));
    // The time of the end of a reservation is one hour after the beginning of the reservations itself
    toDate.setHours(toDate.getHours() + 1);
    let toDateString = formatTimestamp(toDate.getTime());

    // Sum all the reserved tables in that date range
    let countReservedTablesQuery = `SELECT SUM(tables_number) FROM ${TOT_RESERVATION} WHERE date_start BETWEEN '${fromDateString}' AND '${toDateString}'`;

    client.query(countReservedTablesQuery)
    .then((results) => {
      let reservedTables = (results.rows[0].sum || 0);
      let remainingAvailableTables = ALL_TABLES - reservedTables;

      // Check whether ALL_TABLES - the number of reserved tables is greater than or equal to [requiredTables]
      // If not so, then reject the request
      if (remainingAvailableTables < requiredTables) throw new Error("There are not enough available tables for this date");

      // Create the reservation
      let creationalQuery = `INSERT INTO ${TOT_RESERVATION} (user_email, tables_number, guests, date_start, date_end)
      VALUES ('${[userEmail, requiredTables, guests, fromDateString, toDateString].join("','")}')
      RETURNING id`;
      return client.query(creationalQuery);
    })
    .then((results) => {
      res.send(results.rows[0]);
    })
    .catch((error) => {
      sendErrorResponse(res, `Request failed. ${error.message}`);
    });
  }
});

(async () => {
  try {
    await client.connect();
    app.listen(PORT, () => {
      console.log("Started at http://localhost:%d", PORT);
    });
  } catch(error) {
    console.log(error);
  }
})();

function formatTimestamp(timestamp) {
  let date = new Date(timestamp);
  return `${date.getFullYear()}-${(date.getMonth()+1)}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}

function sendErrorResponse(res, message) {
  res.status(401).send(message);
}

function isReservationDateValid(date) {
  return (date && new Date(date).getHours() >= OPENING_TIME_HOUR && new Date(date).getHours() <= LAST_AVAILABLE_TIME_HOUR);
}

function isGuestsNumberValid(guests) {
  return (guests || 0) >= 1 && guests <= ALL_SEATS;
}
