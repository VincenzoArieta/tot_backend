# tot_backend
A simple Docker app which implements a table reservation backend API powered by Express and PostgreSQL.

# RESTful API Endpoints

All the below endpoints are available at http://localhost:8080

- GET /getReservations
- POST /creteUser
- POST /createReservation

# Additional Features

A pgadmin service has been added to the project in order to make the debugging easier.
It will be available at http://localhost:5433

# Run

```bash
docker-compose up --build
```

# Constraints
These are all the validation rules added by myself in addition to the ones listed by the exercise:

- A reservation lasts one hour only

# Assumptions
These are all assumptions that I have made to speed the development job up:

- The time of the reservation is a valid future date such that its hours are multiple of 60 minutes (e.g. 19:00, 20:00)
- The dates of the requests are in UTC format
- A system for managing concurrency access to the resources is implemented
