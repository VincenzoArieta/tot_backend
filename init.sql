
DROP TABLE IF EXISTS tot_user;
DROP TABLE IF EXISTS tot_table;
DROP TABLE IF EXISTS tot_reservation;

CREATE TABLE IF NOT EXISTS tot_user (email VARCHAR(80) PRIMARY KEY, first_name VARCHAR(80), last_name VARCHAR(80));
CREATE TABLE IF NOT EXISTS tot_table (tot_id SERIAL PRIMARY KEY, seats INTEGER DEFAULT 4 NOT NULL);
CREATE TABLE IF NOT EXISTS tot_reservation (
  id SERIAL,
  tables_number INTEGER,
  guests INTEGER,
  date_start TIMESTAMP,
  date_end TIMESTAMP,
  user_email VARCHAR(80) REFERENCES tot_user,
  PRIMARY KEY(id)
);

INSERT INTO tot_user (email, first_name, last_name)
VALUES
  ('john_doe@test.com', 'John', 'Doe'),
  ('foo_smith@test.com', 'Foo', 'Smith'),
  ('sam_watson@test.com', 'Sam', 'Watson');

INSERT INTO tot_table (seats)
VALUES
  (4),
  (4),
  (4),
  (4),
  (4);

INSERT INTO tot_reservation (user_email, guests, tables_number, date_start, date_end)
VALUES
  ('john_doe@test.com', 3, 1, '2021-07-20 19:00', '2021-07-20 20:00'),
  ('john_doe@test.com', 6, 2, '2021-07-21 19:00', '2021-07-21 20:00'),
  ('foo_smith@test.com', 2, 1, '2021-07-20 20:00', '2021-07-20 21:00'),
  ('foo_smith@test.com', 1, 1, '2021-07-21 20:00', '2021-07-21 21:00'),
  ('sam_watson@test.com', 15, 4, '2021-07-20 19:00', '2021-07-20 20:00');
