# syntax=docker/dockerfile:1

FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm ci

# Bundle app source
COPY . .

EXPOSE 8080 5432
CMD [ "node", "index.js" ]
